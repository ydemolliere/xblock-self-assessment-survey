Self Assessment Survey XBlock
=========

## Description ##
This XBlock implements a rating scale style questionaire. A student can respond to statements or questions by selecting from a range of 2 or more options. Upon submission, the student's responses are compared to a set of pre-configured profiles. 


#### Student View - Survey ####
![Student View - Survey](https://bytebucket.org/alerner2/xblock-self-assessment-survey/raw/87a99a1a003a8824bb765fe59b85bf3bbf58a085/images/SurveyQuestions.png)

#### Student View - Survey Results after submission ####
![Student View - Results](https://bytebucket.org/alerner2/xblock-self-assessment-survey/raw/e42a80793e246d634cf27296c04e8632803240bd/images/SurveyResult.png)

## Features ##
- Questions/Statements grouped by categories.
- Configurable response scale
- Simple rule based profile scoring

## Installation ##
- Clone this repository into an appropriate directory on your application server

- Install as normal ([see XBlock documentation](https://github.com/edx/edx-documentation/blob/master/en_us/developers/source/extending_platform/xblocks.rst#testing))

- Ensure that the ALLOW_ALL_ADVANCED_COMPONENTS is set to true.

- Add "selfassessmentsurvey" to the _Advanced Module List_ of any course that uses it.

## Usage ##

### Studio Configuration ###
```json
{
    "meta": {
        "description": "Student Self Assessment",
        "title": "Self Assessment Survey",
        "instruction_text": "Rate your proficiency in the following areas, where 1 is the lowest and 4 the highest"
    },
    "rating_labels": [{"score":-1,"label":"N/A"},{"score":0,"label":"0"},{"score":1,"label":"1"},{"score":2,"label":"2"},{"score":3,"label":"3"},{"score":4,"label":"4"}], 
    "categories": [
        {
        "name": "Project Management Module",
        "questions": [
        	{
        		"id": "q1",
        		"text": "Assignments"
        	},
            {
                "id": "q2",
                "text": "Estimating"
            },
            {
                "id": "q3",
                "text": "Planning"
            }
        ]
        },
        {
        "name": "Demand Management Module",
        "questions": [
            {
                "id": "q4",
                "text": "Unplanned Work"
            },
            {
                "id": "q5",
                "text": "Ideas"
            }
        ]
        }
    ], 
    "profile_definitions": [
        {
            "name": "Profile 1",
            "targets": {"q5": 2, "q4": 2, "q3": 2, "q2": 2, "q1": 2}
        },
        {
            "name": "Profile 2",
            "targets": {"q5": 3, "q4": 3, "q3": 3, "q2": 3, "q1": 3}
        },
        {
            "name": "Profile 3",
            "targets": {"q5": 4, "q4": 4, "q3": 4, "q2": 4, "q1": 4}
        }
    ]
}
```

### Student Usage ###

## TODO ##
- Enhance Studio editor
- Make Categories optional
- Add some css

## License ##

The code in this repository is licensed under version 3 of the AGPL unless
otherwise noted.
