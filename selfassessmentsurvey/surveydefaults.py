DEFAULT_SURVEY = '''
{
    "meta": {
        "description": "Student Self Assessment",
        "title": "Self Assessment Survey",
        "instruction_text": "Rate your proficiency in the following areas, where 1 is the lowest and 4 the highest"
    },
    "rating_labels": [{"score":-1,"label":"N/A"},{"score":0,"label":"0"},{"score":1,"label":"1"},{"score":2,"label":"2"},{"score":3,"label":"3"},{"score":4,"label":"4"}], 
    "categories": [
        {
        "name": "Project Management Module",
        "questions": [
        	{
        		"id": "q1",
        		"text": "Assignments"
        	},
            {
                "id": "q2",
                "text": "Estimating"
            },
            {
                "id": "q3",
                "text": "Planning"
            }
        ]
        },
        {
        "name": "Demand Management Module",
        "questions": [
            {
                "id": "q4",
                "text": "Unplanned Work"
            },
            {
                "id": "q5",
                "text": "Ideas"
            }
        ]
        }
    ], 
    "profile_definitions": [
        {
            "name": "Profile 1",
            "targets": {"q5": 2, "q4": 2, "q3": 2, "q2": 2, "q1": 2}
        },
        {
            "name": "Profile 2",
            "targets": {"q5": 3, "q4": 3, "q3": 3, "q2": 3, "q1": 3}
        },
        {
            "name": "Profile 3",
            "targets": {"q5": 4, "q4": 4, "q3": 4, "q2": 4, "q1": 4}
        }
    ]
}
'''
